<?php 

echo ("<font face=calibri size=3 color=#00BFFF><b>");
echo "Nama : Muhammad Khoirrur Rizqi<br>";
echo "NIM  : 2100018385<br>";
echo "Kelas : H";
echo "<h1>TUGAS 09 PWEB</h1>";
echo "<hr>";

echo ("<font face=times new roman size=3 color=#191970><b>");
echo "<h2>PROG 9.1</h2>";
echo "Uraian Gaji : <br>";
$gaji = 1000000;
$pajak = 0.1; // == 10%
$bonus = 300000;
$thp = $gaji - ($gaji*$pajak);
$hasil = $thp + $bonus;

echo "Gaji sebelum Pajak = Rp. $gaji <br>";
echo "Pajak = $pajak/10%<br>";
echo "Bonus = Rp. $bonus<br>";
echo "Gaji yang dibawa pulang = Rp. $thp<br>";
echo "Gaji yang diterima setelah mendapat Bonus = Rp. $hasil<br>";
echo "<hr>";

echo "<h2>PROG 9.2</h2>";

$a = 5;
$b = 4;

echo "<h3>Program Operator Logika</h3>";
echo "a = $a<br>";
echo "b = $b<br>";
echo " $a == $b : ". ($a == $b);
echo "<br>$a != $b : ". ($a != $b);
echo "<br>$a > $b : ". ($a > $b);
echo "<br>$a < $b : ". ($a < $b);
echo "<br>($a == $b) && ($a > $b) : ".(($a != $b) && ($a > $b));
echo "<br>($a == $b) || ($a > $b) : ".(($a != $b) || ($a > $b));
echo "<br>";
echo "<br>";
echo "Jika : <br>";
echo "Hasil : 0 -> False<br>";
echo "Hasil : 1 -> True";
echo "<hr>";

?>